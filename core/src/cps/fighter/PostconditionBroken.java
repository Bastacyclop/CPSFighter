package cps.fighter;

public class PostconditionBroken extends RuntimeException {
    public PostconditionBroken(String description) {
        super(description);
    }
}
