package cps.fighter;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import cps.fighter.impl.Engine;
import cps.fighter.impl.characters.Jumbo;
import cps.fighter.impl.characters.Reino;
import cps.fighter.kernel.CharacterService;
import cps.fighter.kernel.Command;
import cps.fighter.kernel.EngineService;
import cps.fighter.kernel.RectangleHitboxService;

public class Game extends ApplicationAdapter {
	public EngineService engine;
	public ShapeRenderer renderer;
	public SpriteBatch batch;
	public BitmapFont font;

	float accumulator;
	long marker;
	float step;

	@Override
	public void create () {
		Gdx.graphics.setWindowedMode(800, 600);
		Gdx.graphics.setTitle("BlockImpact");
	    engine = Engine.create(Jumbo.create(), Reino.create());
	    engine.init(800, 600, 200);
	    renderer = new ShapeRenderer();
	    batch = new SpriteBatch();
	    font = new BitmapFont(Gdx.files.internal("InputMono.fnt"));
        font.getRegion().getTexture()
                .setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
	    accumulator = 0;
	    marker = System.nanoTime();
	    step = 1f / 60f;

	    Config.describe();
	}

	@Override
	public void render () {
        long now = System.nanoTime();
        float delta = ((float)(now - marker)) / 1_000_000_000f;
        accumulator += delta;
        marker = now;

        while (accumulator >= step) {
            if (!engine.isGameOver()) { step(); }
            accumulator -= step;
        }

        Gdx.gl.glClearColor(0.266f, 0.266f, 0.266f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderLife(engine.character(0), 50, -1, 560, 300, 20);
		renderLife(engine.character(1), 50, 1, 560, 300, 20);
		renderStun(engine.character(0), 150, -1, 530, 200, 10);
		renderStun(engine.character(1), 150, 1, 530, 200, 10);
        renderer.end();
        engine.character(0).render(this);
        engine.character(1).render(this);

        batch.begin();
        if (engine.isGameOver()) {
            font.setColor(0.9f, 0.9f, 0.9f, 1);
            font.getData().setScale(1);
            String result;
            if (!engine.character(0).isDead()) {
                result = engine.character(0).toString() + " wins!";
            } else if (!engine.character(1).isDead()) {
                result = engine.character(1).toString() + " wins!";
            } else {
                result = "draw!";
            }
            font.draw(batch, result.toUpperCase(), 250, 320);
        }
        font.setColor(0.3f, 0.3f, 0.3f, 1);
        font.getData().setScale(0.3f);
        font.draw(batch, engine.character(0).toString(), 60, 575);
        font.draw(batch, engine.character(1).toString(), 460, 575);
        batch.end();
	}

	public void renderLife(CharacterService c, int offset, int direction, int y, int w, int h) {
		renderer.setColor(0.5f, 0.5f, 0.5f, 0.5f);
		int x  = (direction > 0) ? (engine.sceneWidth() - w - offset) : (offset);
		renderer.rect(x, y, w, h);

		float ratio = (float)(c.life().value()) / (float)(c.life().limit());
		if (ratio < 0.15) {
			renderer.setColor(0.9f, 0.1f, 0.1f, 1);
		} else if (ratio < 0.45) {
			renderer.setColor(0.9f, 0.6f, 0.1f, 1);
		} else {
			renderer.setColor(0.1f, 0.9f, 0.1f, 1);
		}

		int empty = (int)((float)(w) * (1.0 - ratio));
		x += (direction > 0) ? (0) : (empty);
		w -= empty;
		renderer.rect(x + 2, y + 2, Math.max(0, w - 4), Math.max(0, h - 4));
	}


	public void renderStun(CharacterService c, int offset, int direction, int y, int w, int h) {
		renderer.setColor(0.5f, 0.5f, 0.5f, 0.5f);
		int x  = (direction > 0) ? (engine.sceneWidth() - w - offset) : (offset);
		renderer.rect(x, y, w, h);

		float ratio = (float)(c.stun().value()) / (float)(c.stun().limit());
		if (ratio > 0.99) {
            renderer.setColor(0.8f, 0.2f, 0.2f, 1);
        } else {
            renderer.setColor(0.2f, 0.2f, 0.2f, 1);
        }
		int empty = (int)((float)(w) * (1.0 - ratio));
		x += (direction > 0) ? (0) : (empty);
		w -= empty;
		renderer.rect(x + 2, y + 2, Math.max(0, w - 4), Math.max(0, h - 4));
	}

	public void renderHitbox(RectangleHitboxService h) {
		renderer.rect(h.positionX(), h.positionY(), h.width(), h.height());
	}
	
	public void renderEye(CharacterService c) {
		RectangleHitboxService h = c.hitbox();
		int direction = (c.isFacingRight() ^ c.isDown()) ? (1) : (-1);
        renderer.rect(h.positionX() + h.width() / 2 + direction * h.width() / 4,
        		h.positionY() + 2 * h.height() / 3,
        		h.width() / 10,
        		h.height() / 10);
	}
	
	@Override
	public void dispose() {
	    renderer.dispose();
        batch.dispose();
        font.dispose();
	}

	//TODO: Shadowing Resolution
	void step() {
		Command a = Command.IDLE;
		boolean a_can_combo = true;
		if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            a = Command.PUNCH;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.F)) {
            a = Command.KICK;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.Z)) {
            a = Command.UP;
		} else if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
			a = Command.UPLEFT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.E)) {
			a = Command.UPRIGHT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.Q)) {
            a = Command.LEFT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            a = Command.RIGHT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.S)) {
            a = Command.DOWN;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.W)) {
            a = Command.DOWNLEFT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.C)) {
            a = Command.DOWNRIGHT;
        } else {
		    if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
                a = Command.LEFT;
            } else if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                a = Command.RIGHT;
            } else if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                a = Command.DOWN;
            } else if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                a = Command.DOWNLEFT;
            } else if (Gdx.input.isKeyPressed(Input.Keys.C)) {
                a = Command.DOWNRIGHT;
            }
            a_can_combo = false;
        }

        Command b = Command.IDLE;
        boolean b_can_combo = true;
		if (Gdx.input.isKeyJustPressed(Input.Keys.U)) {
            b = Command.PUNCH;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.J)) {
            b = Command.KICK;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.O)) {
            b = Command.UP;
		} else if (Gdx.input.isKeyJustPressed(Input.Keys.I)) {
			b = Command.UPLEFT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.P)) {
			b = Command.UPRIGHT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.K)) {
            b = Command.LEFT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.M)) {
            b = Command.RIGHT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.L)) {
            b = Command.DOWN;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.COMMA)) {
            b = Command.DOWNLEFT;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.SLASH) ||
                   Gdx.input.isKeyJustPressed(Input.Keys.COLON)) {
            b = Command.DOWNRIGHT;
        } else {
		    if (Gdx.input.isKeyPressed(Input.Keys.K)) {
                b = Command.LEFT;
            } else if (Gdx.input.isKeyPressed(Input.Keys.M)) {
                b = Command.RIGHT;
            } else if (Gdx.input.isKeyPressed(Input.Keys.L)) {
                b = Command.DOWN;
            } else if (Gdx.input.isKeyPressed(Input.Keys.COMMA)) {
                b = Command.DOWNLEFT;
            } else if (Gdx.input.isKeyPressed(Input.Keys.SLASH) ||
                       Gdx.input.isKeyPressed(Input.Keys.COLON)) {
                b = Command.DOWNRIGHT;
            }
            b_can_combo = false;
        }

        engine.step(a, a_can_combo, b, b_can_combo);
    }
}
