package cps.fighter;

public class PreconditionBroken extends RuntimeException {
    public PreconditionBroken(String description) {
        super(description);
    }
}
