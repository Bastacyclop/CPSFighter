package cps.fighter;

public interface Clone<T> {
    T cloneWithoutException();
}
