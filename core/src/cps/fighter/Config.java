package cps.fighter;

public class Config {
    public static final boolean CONTRACTS_ENABLED = true;

    public static void describe() {
        if (CONTRACTS_ENABLED) {
            System.out.println("--- contracts are enabled ---");
        }
    }
}
