package cps.fighter;

public interface Renderable {
    void render(Game g);
}
