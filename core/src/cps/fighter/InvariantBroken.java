package cps.fighter;

public class InvariantBroken extends RuntimeException {
        public InvariantBroken(String description) {
            super(description);
        }
}
