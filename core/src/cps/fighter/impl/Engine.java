package cps.fighter.impl;

import cps.fighter.Config;
import cps.fighter.kernel.*;

public class Engine implements EngineService {
    int scene_width = 0;
    int scene_height = 0;
    CharacterService[] character = { null, null };

    public Engine(CharacterService c0, CharacterService c1) {
        character[0] = c0;
        character[1] = c1;
    }

    public static EngineService create(CharacterService c0, CharacterService c1) {
        Engine e = new Engine(c0, c1);
        if (Config.CONTRACTS_ENABLED) {
            return new EngineContract(e);
        } else {
            return e;
        }
    }

    @Override
    public int sceneWidth() {
        return scene_width;
    }

    @Override
    public int sceneHeight() {
        return scene_height;
    }

    @Override
    public CharacterService character(int c) {
        return character[c];
    }

    @Override
    public boolean isGameOver() {
        return character[0].isDead() || character[1].isDead();
    }

    @Override
    public void init(int w, int h, int space) {
        scene_width = w;
        scene_height = h;

        character[0].init(w/2 - space/2, 0, this, 1);
        character[1].init(w/2 + space/2, 0, this, 0);
    }

    @Override
    public void step(Command c0, boolean cc0, Command c1, boolean cc1) {
        int past_y0 = character[0].hitbox().positionY();
        int past_y1 = character[1].hitbox().positionY();
        applyGravity();
        handleSceneCollision(character[0]);
        handleSceneCollision(character[1]);
        character[0].step(c0, cc0);
        character[1].step(c1, cc1);
        handleCharactersCollision(past_y0, past_y1);
        handleSceneCollision(character[0]);
        handleSceneCollision(character[1]);
    }

    private void applyGravity() {
        character[0].move(0, -4);
        character[1].move(0, -4);
    }

    void handleCharactersCollision(int past_y0, int past_y1) {
        RectangleHitboxService h0 = character[0].hitbox();
        RectangleHitboxService h1 = character[1].hitbox();

        if (h0.intersects(h1)) {
            int h0ix = h0.positionX();
            int h0iy = h0.positionY();
            int h0sx = h0ix + h0.width();
            int h0sy = h0iy + h0.height();
            int h0cx = h0ix + h0sx / 2;
            int h0cy = h0iy + h0sy / 2;
            int h1ix = h1.positionX();
            int h1iy = h1.positionY();
            int h1sx = h1ix + h1.width();
            int h1sy = h1iy + h1.height();
            int h1cx = h1ix + h1sx / 2;
            int h1cy = h1iy + h1sy / 2;

            if (past_y0 > (past_y1 + h1.height())) {
                int d = h1sy - h0iy + 1;
                character[0].move(0, d);
            } else if (past_y1 > (past_y0) + h0.height()) {
                int d = h0sy - h1iy + 1;
                character[1].move(0, d);
            } else {
                if (h0cx < h1cx) {
                    int d = h0sx - h1ix + 1;
                    int fhd = d/2;
                    int chd = fhd + d%2;
                    character[0].move(-chd, 0);
                    character[1].move(fhd, 0);
                } else {
                    int d = h1sx - h0ix + 1;
                    int fhd = d/2;
                    int chd = fhd + d%2;
                    character[0].move(fhd, 0);
                    character[1].move(-chd, 0);
                }
            }
        }

    }

    // TODO: width(hitbox(character(E, 0))) + width(hitbox(character(E, 1))) < width(E)
    // TODO: height(hitbox(character(E, 0))) + height(hitbox(character(E, 1))) < height(E)
    void handleSceneCollision(CharacterService c) {
        int ix = c.hitbox().positionX();
        int iy = c.hitbox().positionY();
        int sx = ix + c.hitbox().width();
        int sy = iy + c.hitbox().height();

        RectangleHitboxService oh = c.opponent().hitbox();

        int mx = 0;
        int omx = 0;
        if (ix < 0) {
            omx = c.hitbox().width()+1 - oh.positionX();
            mx = -ix;
        } else if (sx > scene_width) {
            omx = sceneWidth() - c.hitbox().width() - oh.width()-1 - oh.positionX();
            mx = scene_width - sx;
        }

        int my = 0;
        int omy = 0;
        if (iy < 0) {
            omy = c.hitbox().height()+1 - oh.positionY();
            my = -iy;
        } else if (sy > scene_height) {
            omy = sceneHeight() - c.hitbox().height() - oh.height()-1 - oh.positionY();
            my = scene_height - sy;
        }

        c.move(mx, my);

        // What is more appropriate: each dimension alone or both ?
//        if (!(c.hitbox().positionX() <= (oh.positionX() + oh.width()) &&
//                oh.positionX() <= (c.hitbox().width() + c.hitbox().positionX()))) {
//            omy = 0;
//        }
//        if (!(c.hitbox().positionY() <= (oh.positionY() + oh.height()) &&
//                oh.positionY() <= (c.hitbox().positionY() + c.hitbox().height()))) {
//            omx = 0;
//        }

        if (c.hitbox().intersects(oh)) {
            c.opponent().move(omx, omy);
        }
    }
}
