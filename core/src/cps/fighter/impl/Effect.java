package cps.fighter.impl;

import cps.fighter.Config;
import cps.fighter.kernel.EffectContract;
import cps.fighter.kernel.EffectService;
import cps.fighter.kernel.EffectType;

public class Effect implements EffectService{
    EffectType type;
    int value;

    @Override
    public EffectType type() {
        return type;
    }

    @Override
    public int value() {
        return value;
    }

    @Override
    public void init(EffectType t, int v) {
        type = t;
        value = v;
    }

    public static EffectService create() {
        Effect e = new Effect();
        if (Config.CONTRACTS_ENABLED) {
            return new EffectContract(e);
        } else {
            return e;
        }
    }

    public static EffectService create(EffectType t, int v) {
        EffectService e = create();
        e.init(t, v);
        return e;
    }
}
