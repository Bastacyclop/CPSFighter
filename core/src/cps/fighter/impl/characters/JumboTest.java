package cps.fighter.impl.characters;

import org.junit.jupiter.api.BeforeEach;

public class JumboTest extends cps.fighter.kernel.JumboTest {
    @Override
    @BeforeEach
    public void beforeEach() {
        setup(Jumbo.create_with_contract());
    }
}
