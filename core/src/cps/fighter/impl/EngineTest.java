package cps.fighter.impl;

import cps.fighter.impl.characters.Jumbo;
import org.junit.jupiter.api.BeforeEach;

public class EngineTest extends cps.fighter.kernel.EngineTest {
    @Override
    @BeforeEach
    public void beforeEach() {
        setup(new Engine(new Jumbo(), new Jumbo()));
    }
}
