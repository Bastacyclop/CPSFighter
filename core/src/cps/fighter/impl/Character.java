package cps.fighter.impl;

import cps.fighter.Config;
import cps.fighter.kernel.*;

public abstract class Character implements CharacterService {
    protected int[] position = {0, 0};
    protected RectangleHitboxService hitbox = null;
    protected GaugeService life;
    protected GaugeService stun;
    protected int speed;
    protected boolean is_facing_right;
    protected boolean is_down;
    protected boolean is_protected;
    protected EngineService engine = null;
    protected int opponent_id;
    protected CommandStackService cmd_stack;
    protected int fx;
    protected RectangleHitboxService skill_hitbox;
    protected int time_left;
    protected boolean touched;
    protected String name;

    public Character(String name, GaugeService life, GaugeService stun, int speed, CommandStackService cs) {
        this.name = name;
        this.life = life;
        this.stun = stun;
        this.speed = speed;
        this.cmd_stack = cs;
    }

    public String toString() {
        return name;
    }

    @Override
    public int positionX() {
        return position[0];
    }

    @Override
    public int positionY() {
        return position[1];
    }

    @Override
    public RectangleHitboxService hitbox() {
        return hitbox;
    }

    @Override
    public GaugeService life() {
        return life;
    }

    @Override
    public GaugeService stun() {
        return stun;
    }

    @Override
    public int speed() {
        return speed;
    }

    @Override
    public boolean isDown() {
        return is_down;
    }

    @Override
    public boolean isProtected() {
        return is_protected;
    }

    @Override
    public boolean isGrounded() {
        return (hitbox().positionY() <= 0 || isOnOpponentHead());
    }
    
    public boolean isOnOpponentHead() {
    	RectangleHitboxService h = hitbox;
    	RectangleHitboxService o = opponent().hitbox();
    	return (h.positionX() < o.positionX() + o.width() &&
    			o.positionX() < h.positionX() + h.width() &&
    			h.positionY() < (o.positionY() + o.height() + 2));
    }

    @Override
    public boolean isFacingRight() {
        return is_facing_right;
    }

    @Override
    public boolean isDead() {
        return life.value() <= 0;
    }

    @Override
    public EngineService engine() {
        return engine;
    }

    @Override
    public CharacterService opponent() {
        return engine.character(opponent_id);
    }

    @Override
    public CommandStackService cmdStack() {
        return cmd_stack;
    }

    @Override
    public void init(int x, int y, EngineService e, int oid) {
        position[0] = x;
        position[1] = y;
        is_down = false;
        is_protected = false;
        engine = e;
        opponent_id = oid;
        cmd_stack.init(10);
    }

    @Override
    public void move(int x, int y) {
        position[0] += x;
        position[1] += y;
        hitbox.moveTo(hitbox.positionX() + x, hitbox.positionY() + y);
    }

    @Override
    public void applyEffect(EffectService e) {
        switch (e.type()) {
            case DAMAGE: if (!isProtected()) {
                life.add(-e.value());
            } break;

            case STUN: if (!isProtected()) {
                stun.add(e.value());
            } else {
                stun.add(e.value()/2);
            } break;

            case PUSH: move(e.value(), 0);
                break;

            case LOW: if (!(isProtected() && isCrouched())) {
                life.add(-e.value());
            } break;

            case OVERHEAD: if (!isProtected() || isCrouched()) {
                life.add(-e.value());
            } break;

            case MAGIC: life.add(-e.value());
                break;

            case KNOCKDOWN:
                is_down = true;
                is_protected = false;
                break;
        }
    }

    @Override
    public void step(Command c, boolean can_combo) {
        cmd_stack.push(c, can_combo);
        if (stun.value() == stun.limit()) {
            mayGotoStun();
        }
        if (is_down) {
            mayGotoDown();
        }
        step();
        stun.add(-stun.limit()/(60*3));
        if (canMove()) {
        	is_facing_right = positionX() < opponent().positionX();
        }
    }

    public abstract void step();
    public abstract void mayGotoStun();
    public abstract void mayGotoDown();

    public void setSkillHitbox(int width, int height, int offsetx, int offsety) {
        skill_hitbox = skillHitbox(width, height, offsetx, offsety);
    }

    public RectangleHitboxService characterHitbox(int cx, int y, int w, int h) {
        return RectangleHitbox.create(cx, y, w, h);
    }

    public RectangleHitboxService skillHitbox(int width, int height, int offsetx, int offsety) {
        if (isFacingRight()) {
            return RectangleHitbox.create(positionX() + offsetx + width / 2,
                    positionY() + offsety, width, height);
        } else {
            return RectangleHitbox.create(positionX() - offsetx - width / 2,
                    positionY() + offsety, width, height);
        }
    }
}
