package cps.fighter.impl;

import cps.fighter.Config;
import cps.fighter.kernel.HitboxService;
import cps.fighter.kernel.RectangleHitboxContract;
import cps.fighter.kernel.RectangleHitboxService;

import java.util.Random;

public class RectangleHitbox implements RectangleHitboxService {
    int[] inf = {0, 0};
    int[] sup = {1, 1};

    public static RectangleHitboxService create() {
        RectangleHitbox h = new RectangleHitbox();
        if (Config.CONTRACTS_ENABLED) {
            return new RectangleHitboxContract(h, new Random());
        } else {
            return h;
        }
    }

    public static RectangleHitboxService create(int cx, int y, int width, int height) {
        RectangleHitboxService h = create();
        h.init(cx - width/2, y);
        h.resize(width, height);
        return h;
    }

    public int positionX() {
        return inf[0];
    }

    @Override
    public int positionY() {
        return inf[1];
    }

    @Override
    public int width() {
        return sup[0] - inf[0];
    }

    @Override
    public int height() {
        return sup[1] - inf[1];
    }

    @Override
    public boolean contains(int x, int y) {
        return inf[0] <= x && x < sup[0] &&
                inf[1] <= y && y < sup[1];
    }

    @Override
    public boolean intersects(HitboxService other) {
        if (other instanceof RectangleHitboxService) {
            RectangleHitboxService o = (RectangleHitboxService)(other);
            int[] oinf = { o.positionX(), o.positionY() };
            int[] osup = { oinf[0] + o.width(), oinf[1] + o.height() };
            return inf[0] <= osup[0] && oinf[0] <= sup[0] &&
                    inf[1] <= osup[1] && oinf[1] <= sup[1];
        } else {
            throw new RuntimeException("unimplemented");
        }
    }

    @Override
    public boolean equals(HitboxService other) {
        if (other instanceof RectangleHitboxService) {
            RectangleHitboxService o = (RectangleHitboxService)(other);
            return positionX() == o.positionX() &&
                    positionY() == o.positionY() &&
                    width() == o.width() &&
                    height() == o.height();
        } else {
            throw new RuntimeException("unimplemented");
        }
    }

    @Override
    public void init(int x, int y) {
        moveTo(x, y);
    }

    @Override
    public void moveTo(int x, int y) {
        sup[0] += x - inf[0];
        sup[1] += y - inf[1];
        inf[0] = x;
        inf[1] = y;
    }

    @Override
    public void resize(int w, int h) {
        sup[0] = inf[0] + w;
        sup[1] = inf[1] + h;
    }
}
