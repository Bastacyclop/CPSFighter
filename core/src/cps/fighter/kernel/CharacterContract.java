package cps.fighter.kernel;

import cps.fighter.Game;
import cps.fighter.InvariantBroken;
import cps.fighter.PostconditionBroken;
import cps.fighter.PreconditionBroken;
import cps.fighter.impl.RectangleHitbox;

public class CharacterContract implements CharacterService {
    protected CharacterService inner;

    public CharacterContract(CharacterService inner) {
        this.inner = inner;
    }

    public String toString() {
        return inner.toString();
    }

    boolean equal_hitbox(RectangleHitboxService a, RectangleHitboxService b) {
        return (a.height() == b.height() && a.width() == b.width() &&
                a.positionY() == b.positionY() && a.positionX() == b.positionX());
    }

    void checkInvariants() {
        if (isDead() != (life().value() <= 0)) {
            throw new InvariantBroken("isDead");
        }

        if (isGrounded() != (hitbox().positionY() <= 0 || isOnOpponentHead())) {
            throw new InvariantBroken("isGrounded");
        }

        if (isDown() && isProtected()) {
                throw new InvariantBroken("isDown");
        }

        RectangleHitboxService r = RectangleHitbox.create();
        RectangleHitboxService ch = characterHitbox(5,5,5,5);
        r.init(5 - (5/2), 5);
        r.resize(5, 5);
        //TODO real random generated by my mind
        if (!equal_hitbox(r, ch)) {
            throw new InvariantBroken("characterHitbox");
        }

        RectangleHitboxService sh = skillHitbox(6,6,6,6);
        if (isFacingRight()) {
            RectangleHitboxService hit = characterHitbox(positionX() + 6 + (6/2),
                    positionY() + 6, 6, 6);
            if (!equal_hitbox(hit, sh)) {
                throw new InvariantBroken("skillHitbox");
            }
        } else {
            RectangleHitboxService hit = characterHitbox(positionX() - 6 - (6/2),
                    positionY() + 6, 6, 6);
            if (!equal_hitbox(hit, sh)) {
                throw new InvariantBroken("skillHitbox");
            }
        }
    }

    public boolean isOnOpponentHead() {
        RectangleHitboxService h = hitbox();
        RectangleHitboxService o = opponent().hitbox();
        return (h.positionX() < o.positionX() + o.width() &&
                o.positionX() < h.positionX() + h.width() &&
                h.positionY() < (o.positionY() + o.height() + 2));
    }

    @Override
    public int positionX() {
        return inner.positionX();
    }

    @Override
    public int positionY() {
        return inner.positionY();
    }

    @Override
    public EngineService engine() {
        return inner.engine();
    }

    @Override
    public CharacterService opponent() {
        return inner.opponent();
    }

    @Override
    public CommandStackService cmdStack() {
        return inner.cmdStack();
    }

    @Override
    public RectangleHitboxService characterHitbox(int x, int y, int w, int h) {
        return inner.characterHitbox(x, y, w, h);
    }

    @Override
    public RectangleHitboxService skillHitbox(int w, int h, int ox, int oy) {
        return inner.skillHitbox(w, h, ox, oy);
    }

    @Override
    public RectangleHitboxService hitbox() {
        return inner.hitbox();
    }

    @Override
    public GaugeService life() {
        return inner.life();
    }

    @Override
    public int speed() {
        return inner.speed();
    }

    @Override
    public boolean isDown() {
        return inner.isDown();
    }

    @Override
    public boolean isCrouched() {
        return inner.isCrouched();
    }

    @Override
    public boolean canMove() {
        return inner.canMove();
    }

    @Override
    public boolean isProtected() {
        return inner.isProtected();
    }

    @Override
    public boolean isGrounded() {
        return inner.isGrounded();
    }

    @Override
    public boolean isFacingRight() {
        return inner.isFacingRight();
    }

    @Override
    public boolean isDead() {
        return inner.isDead();
    }

    @Override
    public void init(int x, int y, EngineService e, int oid) {
        if (oid != 0 && oid != 1) {
            throw new PreconditionBroken("init");
        }

        inner.init(x, y, e, oid);
        checkInvariants();

        if (positionX() != x || positionY() != y) {
            throw new PostconditionBroken("position");
        }
        if (stun().value() != 0 || isDead()) {
            throw new PostconditionBroken("state");
        }
        if (engine() != e || opponent() != e.character(oid)) {
            throw new PostconditionBroken("engine");
        }
        //TODO: cmdStack
    }

    @Override
    public void move(int x, int y) {
        int px = positionX();
        int py = positionY();
        int phx = hitbox().positionX();
        int phy = hitbox().positionY();

        inner.move(x, y);
        checkInvariants();

        if (positionX() != (px + x) || positionY() != (py + y)) {
            throw new PostconditionBroken("move position");
        }
        if (hitbox().positionX() != (phx + x) || hitbox().positionY() != (phy + y)) {
            throw new PostconditionBroken("move hitbox");
        }
        //TODO: check with clause
    }

    @Override
    public void step(Command c, boolean can_combo) {
        inner.step(c, can_combo);
        checkInvariants();
    }

    @Override
    public GaugeService stun() {
        return inner.stun();
    }


    @Override
    public void applyEffect(EffectService e) {
        boolean isp = isProtected();
        boolean isc = isCrouched();
        int life = life().value();
        int stun = stun().value();
        int px = positionX();
        int py = positionY();
        int hx = hitbox().positionX();
        int hy = hitbox().positionY();

        inner.applyEffect(e);
        checkInvariants();

        switch (e.type()) {
            case LOW: if (isp && isc) {
                if (life().value() != life) {
                    throw new PostconditionBroken("low life");
                }
            } else {
                if (life().value() != GaugeContract.simulAdd(life, -e.value(), life().limit())) {
                    throw new PostconditionBroken("low life 2");
                }
            } break;

            case OVERHEAD: if (isp && !isc) {
                if (life().value() != life) {
                    throw new PostconditionBroken("overhead life");
                }
            } else {
                if (life().value() != GaugeContract.simulAdd(life, -e.value(), life().limit())) {
                    throw new PostconditionBroken("overhead life 2");
                }
            } break;

            case STUN: if (isp) {
                if (stun().value() != GaugeContract.simulAdd(stun, e.value()/2, stun().limit())) {
                    throw new PostconditionBroken("stun value");
                }
            } else {
                if (stun().value() != GaugeContract.simulAdd(stun, e.value(), stun().limit())) {
                    throw new PostconditionBroken("stun protected");
                }
            } break;

            case PUSH: if ((positionX() != (px + e.value()) || positionY() != py) &&
                    (hitbox().positionX() != (hx + e.value()) || hitbox().positionY() != hy)) {
                throw new PostconditionBroken("push");
            } break;

            case DAMAGE: if (isp) {
                if (life().value() != life) {
                    throw new PostconditionBroken("damage life");
                }
            } else {
                if (life().value() != GaugeContract.simulAdd(life, -e.value(), life().limit())) {
                    throw new PostconditionBroken("damage life 2");
                }
            } break;

            case KNOCKDOWN: if (!isDown()) {
                throw new PostconditionBroken("knockdown");
            } break;
        }
        //TODO: with clause
    }


    @Override
    public void render(Game g) {
        inner.render(g);
    }
}
