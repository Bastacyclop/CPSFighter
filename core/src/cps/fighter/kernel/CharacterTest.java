package cps.fighter.kernel;

import cps.fighter.impl.Engine;
import cps.fighter.impl.characters.Jumbo;
import cps.fighter.impl.characters.Reino;
import static cps.fighter.kernel.Command.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

public abstract class CharacterTest {
    CharacterService inner;
    Random rng;

    public CharacterTest() {
        inner = null;
        rng = new Random();
    }

    protected void setup(CharacterService inner) {
        this.inner = new CharacterContract(inner);
    }

    @BeforeEach
    public abstract void beforeEach();

    @AfterEach
    public void afterEach() {
        this.inner = null;
    }

    @Test
    void testInitOk() {
        EngineService e = Engine.create(new Jumbo(), inner);
        e.init(300, 300, 100);
        inner.init(rng.nextInt(500), 0, e, 0);
    }

    @Test
    void testStepIdle() {
        EngineService e = Engine.create(inner, new Jumbo());
        e.init(300, 300, 100);
        inner.init(rng.nextInt(500), rng.nextInt(500), e, 1);
        inner.step(IDLE, false);
    }

    @Test
    void testStepNotIdle() {
        EngineService e = Engine.create(inner, new Jumbo());
        e.init(300, 300, 100);
        inner.init(rng.nextInt(500), rng.nextInt(500), e, 1);
        inner.step(PUNCH, true);
    }

    @Test
    void testSteps() {
        for (Command c : values()) {
            testInitOk();
            inner.step(IDLE, false);
            inner.step(c, false);
            for (int i = 0; i < 60; i++) {
                inner.step(IDLE, false);
            }
        }
    }

    @Test
    void testBeating() {
        CharacterService[] opponents = { Jumbo.create(), Reino.create() };
        Command[] lreactions = {IDLE, LEFT, DOWN, DOWNLEFT};
        Command[] beatings = {PUNCH, KICK};

        for (CharacterService opponent : opponents) {
            for (Command r : lreactions) {
                for (Command b : beatings) {
                    EngineService e = Engine.create(inner, opponent);
                    e.init(10, 10, 5);
                    e.init(inner.hitbox().width() + opponent.hitbox().width() + 2,
                            300, 10);
                    e.step(IDLE, false, IDLE, false);

                    for (int i = 0; i < 666; i++) {
                        if (e.isGameOver()) {
                            break;
                        }
                        e.step(r, false, b, false);
                    }
                }
            }
        }

        Command[] rreactions = {IDLE, RIGHT, DOWN, DOWNRIGHT};
        for (CharacterService opponent : opponents) {
            for (Command r : rreactions) {
                for (Command b : beatings) {
                    EngineService e = Engine.create(opponent, inner);
                    e.init(10, 10, 5);
                    e.init(inner.hitbox().width() + opponent.hitbox().width() + 2,
                            300, 10);
                    e.step(IDLE, false, IDLE, false);

                    for (int i = 0; i < 666; i++) {
                        if (e.isGameOver()) {
                            break;
                        }
                        e.step(b, false, r, false);
                    }
                }
            }
        }
    }
}
