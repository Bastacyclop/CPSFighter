package cps.fighter.kernel;

/*
    HitboxService Service,
    See the specification
 */
public interface HitboxService {
    int positionX();
    int positionY();
    boolean contains(int x, int y);
    boolean intersects(HitboxService other);
    boolean equals(HitboxService other);

    void init(int x, int y);

    void moveTo(int x, int y);
}
