package cps.fighter.kernel;

import cps.fighter.impl.Engine;
import cps.fighter.impl.characters.Jumbo;
import cps.fighter.impl.characters.Reino;
import org.junit.jupiter.api.Test;
import static cps.fighter.kernel.Command.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class JumboTest extends CharacterTest {
    @Override
    protected void setup(CharacterService inner) {
        this.inner = inner;
    }

    JumboService get() {
        return (JumboService) inner;
    }

    @Test
    void testFireball() {
        CharacterService[] opponents = {Jumbo.create(), Reino.create() };
        Command[] lcombo = {DOWN, DOWNLEFT, LEFT, PUNCH};
        Command[] rcombo = {DOWN, DOWNRIGHT, RIGHT, PUNCH};
        Command[] reaction = {IDLE, LEFT, IDLE, RIGHT, IDLE, IDLE };

        for (CharacterService opponent : opponents) {
            EngineService e = Engine.create(opponent, inner);
            e.init(200, 200, 50);

            e.step(IDLE, false, IDLE, false);

            for (Command c : rcombo) {
                e.step(IDLE, false, c, true);
            }
            assertTrue(get().state() == JumboState.PUNCH, "should be PUNCH");

            for (int i = 0; i < 10; i++) {
                e.step(IDLE, false, IDLE, false);
            }

            for (Command c : lcombo) {
                e.step(IDLE, false, c, true);
            }
            assertTrue(get().state() == JumboState.FIREBALL, "should be FIREBALL");

            for (int i = 0; i < 40; i++) {
                for (Command r : reaction) {
                    e.step(r, false, IDLE, false);
                }
            }
        }

        for (CharacterService opponent : opponents) {
            EngineService e = Engine.create(inner, opponent);
            e.init(200, 200, 50);

            e.step(IDLE, false, IDLE, false);

            for (Command c : lcombo) {
                e.step(c, true, IDLE, false);
            }
            assertTrue(get().state() == JumboState.PUNCH, "should be PUNCH");

            for (int i = 0; i < 10; i++) {
                e.step(IDLE, false, IDLE, false);
            }

            for (Command c : rcombo) {
                e.step(c, true, IDLE, false);
            }
            assertTrue(get().state() == JumboState.FIREBALL, "should be FIREBALL");

            for (int i = 0; i < 40; i++) {
                for (Command r : reaction) {
                    e.step(IDLE, false, r, false);
                }
            }
        }
    }
}
