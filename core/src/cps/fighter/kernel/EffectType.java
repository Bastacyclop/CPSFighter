package cps.fighter.kernel;

public enum EffectType {
    DAMAGE,
    PUSH,
    STUN,
    LOW,
    OVERHEAD,
    MAGIC,
    KNOCKDOWN,
}
