package cps.fighter.kernel;

import cps.fighter.Renderable;

/*
    RectangleHitboxService Service,
    See the specification
 */
public interface RectangleHitboxService extends HitboxService {
    int width();
    int height();

    void resize(int w, int h);
}
