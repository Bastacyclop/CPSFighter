package cps.fighter.kernel;

import cps.fighter.InvariantBroken;
import cps.fighter.PostconditionBroken;
import cps.fighter.PreconditionBroken;

import java.util.Random;

public class RectangleHitboxContract
        extends HitboxContract
        implements RectangleHitboxService {
    public RectangleHitboxContract(RectangleHitboxService inner, Random rng) {
        super(inner, rng);
    }

    public RectangleHitboxService get() {
        return (RectangleHitboxService)(inner);
    }

    @Override
    void checkInvariants() {
        super.checkInvariants();

        if (width() < 0 || height() < 0) {
            throw new InvariantBroken("negative size");
        }
        if (contains(positionX() - 1 - rng.nextInt(99),
                     positionY() - 100 + rng.nextInt(height() + 200)) ||
                contains(positionX() + width() + 1 + rng.nextInt(99),
                         positionY() - 100 + rng.nextInt(height() + 200)) ||
                contains(positionX() - 100 + rng.nextInt(width() + 200),
                         positionY() - 1 - rng.nextInt(99)) ||
                contains(positionX() - 100 + rng.nextInt(width() + 200),
                         positionY() + height() + 1 + rng.nextInt(99))) {
            throw new InvariantBroken("contains");
        }
        if (width() > 0 && height() > 0 &&
                !contains(positionX() + rng.nextInt(width()),
                        positionY() + rng.nextInt(height()))) {
            throw new InvariantBroken("contains");
        }
    }

    @Override
    public int width() { return get().width(); }

    @Override
    public int height() { return get().height(); }

    @Override
    public void resize(int w, int h) {
        int x = positionX();
        int y = positionY();

        if (w < 0 || h < 0) {
            throw new PreconditionBroken("negative size");
        }

        get().resize(w, h);
        checkInvariants();

        if (width() != w || height() != h) {
            throw new PostconditionBroken("size after resize");
        }
        if (positionX() != x || positionY() != y) {
            throw new PostconditionBroken("position after resize");
        }
    }

    // TODO: moveTo
}
