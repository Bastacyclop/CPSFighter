package cps.fighter.kernel;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public abstract class EffectTest {
    EffectService inner;

    public EffectTest() {
        inner = null;
    }

    protected void setup(EffectService inner) {
        this.inner = new EffectContract(inner);
    }

    @BeforeEach
    public abstract void beforeEach();

    @AfterEach
    public final void afterEach() {
        inner = null;
    }

    @Test
    void testInitOk() {
        inner.init(EffectType.KNOCKDOWN, 18);
    }
}
