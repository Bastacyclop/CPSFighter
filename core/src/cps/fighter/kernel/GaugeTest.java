package cps.fighter.kernel;

import cps.fighter.InvariantBroken;
import cps.fighter.PostconditionBroken;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public abstract class GaugeTest {
    protected GaugeService inner;

    public GaugeTest() {
        inner = null;
    }

    protected void setup(GaugeService inner) {
        this.inner = new GaugeContract(inner);
    }

    @BeforeEach
    public abstract void beforeEach();

    @AfterEach
    public final void afterEach() {
        inner = null;
    }

    @Test
    void testInitOk() {
        inner.init(5, 50);
    }

    @Test
    void testInitLesser() {
        try {
            inner.init(-2, 50);
            assert(false);
        } catch (InvariantBroken pb) {
        }
    }

    @Test
    void testInitEqual() {
        inner.init(0, 0);
    }

    @Test
    void testInitGreater() {
        try {
            inner.init(50, 5);
            assert(false);
        } catch (InvariantBroken pb) {
        }
    }

    @Test
    void testAdd() {
        inner.init(5, 50);
        inner.add(5);
    }

    @Test
    void testAddGreater() {
        inner.init(5, 50);
        inner.add(100);
    }

    @Test
    void testSub() {
        inner.init(10, 50);
        inner.add(-5);
    }

    @Test
    void testSubGreater() {
        inner.init(10, 50);
        inner.add(-75);
    }
}
