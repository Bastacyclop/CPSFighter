package cps.fighter.kernel;

import cps.fighter.InvariantBroken;
import cps.fighter.PostconditionBroken;
import cps.fighter.PreconditionBroken;

public class EngineContract implements EngineService {
    protected EngineService inner;

    public EngineContract(EngineService inner) {
        this.inner = inner;
    }

    public void checkInvariants() {
        if (isGameOver() != (character(0).isDead()
                          || character(1).isDead())) {
            throw new InvariantBroken("isGameOver");
        }
    }

    @Override
    public int sceneWidth() {
        return inner.sceneWidth();
    }

    @Override
    public int sceneHeight() {
        return inner.sceneHeight();
    }

    @Override
    public boolean isGameOver() {
        return inner.isGameOver();
    }

    @Override
    public CharacterService character(int c) {
        return inner.character(c);
    }

    @Override
    public void init(int w, int h, int space) {
        if (h < 0 || space < 0 || w <= space) {
            throw new PreconditionBroken("scene layout");
        }

        inner.init(w, h, space);
        checkInvariants();

        if (inner.sceneWidth() != w || inner.sceneHeight() != h) {
            throw new PostconditionBroken("scene size");
        }
        if ((inner.character(0).positionX() != (w/2 - space/2)) ||
                inner.character(1).positionX() != (w/2 + space/2) ||
                inner.character(0).positionY() != 0 ||
                inner.character(1).positionY() != 0) {
            throw new PostconditionBroken("character placement");
        }
        if (inner.character(0).engine() != inner ||
                inner.character(1).engine() != inner) {
            throw new PostconditionBroken("character engine");
        }
    }

    @Override
    public void step(Command c0, boolean cc0, Command c1, boolean cc1) {
        if (isGameOver()) {
            throw new PreconditionBroken("step when game is over");
        }

        inner.step(c0, cc0, c1, cc1);
        checkInvariants();

        if (character(0).hitbox().intersects(character(1).hitbox())) {
            throw new PostconditionBroken("character overlap");
        }

        for (int i = 0; i < 2; i++) {
            if (character(i).hitbox().positionX() < 0 ||
                    character(i).hitbox().positionY() < 0 ||
                    character(i).hitbox().positionX() + character(i).hitbox().width() > sceneWidth() ||
                    character(i).hitbox().positionY() + character(i).hitbox().height() > sceneHeight()) {
                throw new PostconditionBroken("out of scene bounds");
            }
        }
    }
}
