package cps.fighter.kernel;

import cps.fighter.InvariantBroken;
import cps.fighter.PostconditionBroken;
import cps.fighter.PreconditionBroken;

public class CommandStackContract implements CommandStackService {
    protected CommandStackService inner;
    protected int init_size;

    public CommandStackContract(CommandStackService s) {
        this.inner = s;
    }

    void checkInvariants() {
        if (init_size != size()) {
            throw new InvariantBroken("const");
        }
        Command last = last();
        if (!popCombo(new Command[]{}) || last != last()) {
            throw new InvariantBroken("pop empty");
        }
    }

    @Override
    public int size() {
        return inner.size();
    }

    @Override
    public Command last() {
        return inner.last();
    }

    @Override
    public void init(int size) {
        if (size <= 0) {
            throw new PreconditionBroken("init size");
        }

        inner.init(size);
        init_size = size;
        checkInvariants();

        if (size() != size) {
            throw new PostconditionBroken("init size");
        }
        if (last() != Command.IDLE) {
            throw new PostconditionBroken("init last");
        }
        if (popCombo(new Command[]{ Command.DOWN, Command.KICK, Command.KICK }) ||
            popCombo(new Command[]{ Command.PUNCH, Command.UP }) ||
            popCombo(new Command[]{ Command.KICK, Command.DOWNRIGHT, Command.UPRIGHT })) {
            throw new PostconditionBroken("init popCombo");
        }
    }

    @Override
    public void push(Command c, boolean can_combo) {
        inner.push(c, can_combo);
        checkInvariants();

        if (last() != c) {
            throw new PostconditionBroken("push last");
        }
        // TODO
//        if (can_combo && !popCombo(new Command[]{ c })) {
//            throw new PostconditionBroken("push unit combo");
//        }
        // need to clone/copy the stack
    }

    @Override
    public boolean popCombo(Command[] combo) {
        if (combo.length > size()) {
            throw new PreconditionBroken("combo size");
        }
        for (Command c : combo) {
            if (c == Command.IDLE) {
                throw new PreconditionBroken("IDLE in popCombo");
            }
        }


        return inner.popCombo(combo);
    }
}
