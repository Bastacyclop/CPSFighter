package cps.fighter.kernel;

public interface GaugeService {
    int value();
    int limit();

    void init(int v, int l);

    void add(int a);
}
