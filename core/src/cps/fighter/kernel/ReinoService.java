package cps.fighter.kernel;

public interface ReinoService extends CharacterService {
    ReinoState state();
    int fx();
    int timeLeft();
    RectangleHitboxService skillHitbox();
    boolean touched();

    void gotoIdle();
    void gotoJump(int x);
    void gotoMove(int x);
    void gotoCrouch(int x);
    void gotoPunch();
    void gotoKick();
    boolean gotoCharge();
    void stepIdle(Command c);
    void stepMove(Command c);
    void stepJump(Command c);
    void stepCrouch(Command c);
    void stepPunch(Command c);
    void stepKick(Command c);
    void stepCharge(Command c);
    void stepStun(Command c);
}
