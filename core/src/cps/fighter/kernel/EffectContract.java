package cps.fighter.kernel;

import cps.fighter.PostconditionBroken;

public class EffectContract implements EffectService {
    protected EffectService inner;

    public EffectContract(EffectService inner) {
        this.inner = inner;
    }

    @Override
    public EffectType type() {
        return inner.type();
    }

    @Override
    public int value() {
        return inner.value();
    }

    @Override
    public void init(EffectType t, int v) {
        inner.init(t, v);

        if (value() != v|| type() != t) {
            throw new PostconditionBroken("init");
        }
    }
}
