package cps.fighter.kernel;

import cps.fighter.InvariantBroken;
import cps.fighter.PostconditionBroken;

public class GaugeContract implements GaugeService {
    protected GaugeService inner;

    public GaugeContract(GaugeService inner) {
        this.inner = inner;
    }

    void checkInvariants() {
        if (0 > value() || value() > limit()) {
            throw new InvariantBroken("value limits");
        }
    }

    @Override
    public int value() {
        return inner.value();
    }

    @Override
    public int limit() {
        return inner.limit();
    }

    @Override
    public void init(int v, int l) {
        inner.init(v, l);
        checkInvariants();

        if (value() != v || limit() != l) {
            throw new PostconditionBroken("init");
        }
    }

    @Override
    public void add(int a) {
        int v = value();

        inner.add(a);
        checkInvariants();

        if (value() != simulAdd(v, a, limit())) {
            throw new PostconditionBroken("add");
        }
    }

    public static int simulAdd(int v, int a, int l) {
        return Math.min(Math.max(v + a, 0), l);
    }
}
