package cps.fighter.kernel;

public enum ReinoState {
    IDLE,
    CROUCH,
    MOVE,
    PUNCH,
    KICK,
    JUMP,
    STUN,

    CHARGE,
}
