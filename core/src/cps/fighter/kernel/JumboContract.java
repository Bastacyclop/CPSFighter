package cps.fighter.kernel;

import cps.fighter.InvariantBroken;
import cps.fighter.PostconditionBroken;

public class JumboContract
        extends CharacterContract
        implements JumboService {
    public JumboContract(JumboService inner) {
        super(inner);
    }

    public JumboService get() {
        return (JumboService) inner;
    }

    @Override
    void checkInvariants() {
        super.checkInvariants();

        if (isCrouched() != (state() == JumboState.CROUCH)) {
            throw new InvariantBroken("crouch");
        }
        if (canMove() != (state() == JumboState.CROUCH || state() == JumboState.IDLE)) {
            throw new InvariantBroken("can move");
        }
    }

    @Override
    public void init(int x, int y, EngineService e, int oid) {
        super.init(x, y, e, oid);
        checkInvariants();

        if (speed() != 2) {
            throw new PostconditionBroken("speed");
        }
        if (life().value() != 100 || life().limit() != 100) {
            throw new PostconditionBroken("life");
        }
        if (stun().value() != 0 || stun().limit() != 300) {
            throw new PostconditionBroken("stun");
        }
        //TODO: test identity init
    }

    @Override
    public JumboState state() {
        return get().state();
    }

    @Override
    public int fx() {
        return get().fx();
    }

    @Override
    public int timeLeft() {
        return get().timeLeft();
    }

    @Override
    public RectangleHitboxService skillHitbox() {
        return get().skillHitbox();
    }

    @Override
    public boolean touched() {
        return get().touched();
    }

    @Override
    public void gotoIdle() {
        get().gotoIdle();
        checkInvariants();

        if (state() != JumboState.IDLE) {
            throw new PostconditionBroken("idle state");
        }
        if (isProtected()) {
            throw new PostconditionBroken("is protected idle");
        }

        RectangleHitboxService r = characterHitbox(hitbox().positionX(),
                hitbox().positionY(), 50, 80);
        if (r.width() != 50 || r.height() != 80
                || r.positionY() != hitbox().positionY()
                || r.positionX() != (hitbox().positionX() - hitbox().width() / 2)) {
            throw new PostconditionBroken("hitbox");
        }

        //TODO with clause
    }

    @Override
    public void gotoJump(int x) {
        get().gotoJump(x);
        checkInvariants();

        if (state() != JumboState.JUMP) {
            throw new PostconditionBroken("state");
        }
        if (fx() != x) {
            throw new PostconditionBroken("fx");
        }
        if (timeLeft() != 30) {
            throw new PostconditionBroken("time");
        }
    }

    @Override
    public void gotoMove(int x) {
        get().gotoMove(x);
        checkInvariants();

        if (state() != JumboState.MOVE) {
            throw new PostconditionBroken("state");
        }
        if (fx() != x) {
            throw new PostconditionBroken("fx");
        }
        if (isProtected() != ((x != 0) && (isFacingRight() == (x < 0)))) {
            throw new PostconditionBroken("is protected");
        }
    }

    @Override
    public void gotoCrouch(int x) {
        get().gotoCrouch(x);
        checkInvariants();

        if (state() != JumboState.CROUCH) {
            throw new PostconditionBroken("state");
        }
        if (isProtected() != ((x != 0) && (isFacingRight() == (x < 0)))) {
            throw new PostconditionBroken("is protected");
        }

        RectangleHitboxService r = characterHitbox(hitbox().positionX(),
                hitbox().positionY(), 52, 40);
        if (r.width() != 52 || r.height() != 40
                || r.positionY() != hitbox().positionY()
                || r.positionX() != (hitbox().positionX() - hitbox().width() / 2)) {
            throw new PostconditionBroken("hitbox");
        }
        //TODO with clause
    }

    @Override
    public void gotoPunch() {
        RectangleHitboxService skillh = skillHitbox(40, 10, 20, 45);
        get().gotoPunch();
        checkInvariants();

        if (state() != JumboState.PUNCH) {
            throw new PostconditionBroken("state");
        }
        if (timeLeft() != 10) {
            throw new PostconditionBroken("time left");
        }
        if (touched()) {
            throw new PostconditionBroken("touch");
        }

        if (skillh.width() != skillHitbox().width()
                || skillh.height() != skillHitbox().height()
                || skillHitbox().positionX() != skillh.positionX()
                || skillHitbox().positionY() != skillh.positionY())
            {
                throw new PostconditionBroken("skill hitbox");
            }
        //TODO with clause
    }

    @Override
    public void gotoKick() {
        RectangleHitboxService skillh = skillHitbox(60, 10, 20, 10);
        get().gotoKick();
        checkInvariants();

        if (state() != JumboState.KICK) {
            throw new PostconditionBroken("state");
        }
        if (timeLeft() != 30) {
            throw new PostconditionBroken("time left");
        }
        if (touched()) {
            throw new PostconditionBroken("touch");
        }

        if (skillh.width() != skillHitbox().width()
                || skillh.height() != skillHitbox().height()
                || skillHitbox().positionX() != skillh.positionX()
                || skillHitbox().positionY() != skillh.positionY())
        {
            throw new PostconditionBroken("skill hitbox");
        }
        //TODO with clause
    }

    @Override
    public boolean gotoFireball() {
        boolean r = get().gotoFireball();
        checkInvariants();
        return r;
        //TODO
    }

    @Override
    public void stepIdle(Command c) {
        get().stepIdle(c);
        checkInvariants();

        // TODO: more checks
        switch (state()) {
            case IDLE:
            case FIREBALL: // TODO
                break;
            case JUMP: if (!isGrounded() || (
                    c != Command.UP &&
                    c != Command.UPLEFT &&
                    c != Command.UPRIGHT)) {
                throw new PostconditionBroken("state jump");
            } break;
            case MOVE: if (c != Command.LEFT &&
                    c != Command.RIGHT) {
                throw new PostconditionBroken("state move");
            } break;
            case CROUCH: if (c != Command.DOWN &&
                    c != Command.DOWNLEFT &&
                    c != Command.DOWNRIGHT) {
                throw new PostconditionBroken("state crouch");
            } break;
            case PUNCH: if (c != Command.PUNCH) {
                throw new PostconditionBroken("state punch");
            } break;
            case KICK: if (c != Command.KICK) {
                throw new PostconditionBroken("state kick");
            } break;
        }
    }

    @Override
    public void stepMove(Command c) {
        get().stepMove(c);
        checkInvariants();

        //TODO
    }

    @Override
    public void stepJump(Command c) {
        int tl = timeLeft();
        boolean isg = isGrounded();

        get().stepJump(c);
        checkInvariants();

        //TODO: more
        if (tl > 0) {
            if (timeLeft() != tl - 1) {
                throw new PostconditionBroken("timeLeft");
            }
        } else if (isg) {
            if (state() != JumboState.IDLE) {
                throw new PostconditionBroken("jump idle");
            }
        }
    }

    @Override
    public void stepCrouch(Command c) {
        get().stepCrouch(c);
        checkInvariants();

        //TODO
    }

    @Override
    public void stepPunch(Command c) {
        int tl = timeLeft();
        boolean past_touched = touched();
        boolean intersects = skillHitbox().intersects(opponent().hitbox());

        get().stepPunch(c);
        checkInvariants();

        //TODO: more
        if (tl > 0) {
            if (!past_touched) {
                if (intersects) {
                    if (!touched()) {
                        throw new PostconditionBroken("touched");
                    }
                } else if (touched()) {
                    throw new PostconditionBroken("touched");
                }
            } else if (!touched()) {
                throw new PostconditionBroken("touched");
            }
            if (timeLeft() != tl - 1) {
                throw new PostconditionBroken("timeLeft");
            }
        }
    }

    @Override
    public void stepKick(Command c) {
        int tl = timeLeft();
        boolean past_touched = touched();
        boolean intersects = skillHitbox().intersects(opponent().hitbox());

        get().stepKick(c);
        checkInvariants();

        //TODO: more
        if (tl > 0) {
            if (!past_touched) {
                if (intersects) {
                    if (!touched()) {
                        throw new PostconditionBroken("touched");
                    }
                } else if (touched()) {
                    throw new PostconditionBroken("touched");
                }
            } else if (!touched()) {
                throw new PostconditionBroken("touched");
            }
            if (timeLeft() != tl - 1) {
                throw new PostconditionBroken("timeLeft");
            }
        }
    }

    @Override
    public void stepFireball(Command c) {
        int tl = timeLeft();
        int shx = skillHitbox().positionX();
        int shy = skillHitbox().positionY();
        int fx = fx();

        get().stepFireball(c);
        checkInvariants();

        //TODO: more
        if (tl > 0) {
            if ((shx + fx * speed()) != skillHitbox().positionX() &&
                    shy != skillHitbox().positionY()) {
                throw new PostconditionBroken("skillHitbox");
            }
            if (timeLeft() != tl - 1) {
                throw new PostconditionBroken("timeLeft");
            }
        }
    }

    @Override
    public void stepStun(Command c) {
        int tl = timeLeft();

        get().stepStun(c);
        checkInvariants();

        //TODO: more
        if (tl > 0) {
            if (stun().value() != stun().limit()) {
                throw new PostconditionBroken("stun");
            }
            if (timeLeft() != tl - 1) {
                throw new PostconditionBroken("timeLeft");
            }
        } else {
            if (isDown() || stun().value() != 0) {
                throw new PostconditionBroken("stun end");
            }
        }
    }

    @Override
    public void step(Command cmd, boolean cc) {
        boolean fr = isFacingRight();

        super.step(cmd, cc);

        //TODO: more
        if (canMove()) {
            if (isFacingRight() != (positionX() < opponent().positionX())) {
                throw new PostconditionBroken("isFacingRight");
            }
        } else if (isFacingRight() != fr) {
            throw new PostconditionBroken("isFacingRight");
        }
    }

    @Override
    public void move(int x, int y) {
        int shx = skillHitbox().positionX();
        int shy = skillHitbox().positionY();

        super.move(x, y);
        checkInvariants();

       if ((state() == JumboState.KICK || state() == JumboState.PUNCH) &&
               ((skillHitbox().positionX() != (shx + x))
                       || (skillHitbox().positionY() != (shy + y)))) {
           throw new PostconditionBroken("move");
       }
    }
}
