package cps.fighter.kernel;

public enum Command {
    IDLE,
    LEFT,
    RIGHT,
    UP,
    DOWN,
    UPLEFT,
    UPRIGHT,
    DOWNLEFT,
    DOWNRIGHT,
    PUNCH,
    KICK,
}
