package cps.fighter.kernel;

public interface CommandStackService {
    int size();
    Command last();

    void init(int size);

    void push(Command c, boolean can_combo);
    boolean popCombo(Command[] combo);
}
