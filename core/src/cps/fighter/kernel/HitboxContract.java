package cps.fighter.kernel;

import cps.fighter.PostconditionBroken;

import java.util.Random;

public class HitboxContract implements HitboxService {
    HitboxService inner;
    Random rng;

    HitboxContract(HitboxService inner, Random rng) {
        this.inner = inner;
        this.rng = rng;
    }

    void checkInvariants() {
        // we can't check intersects :/
        // TODO: check equals ?
    }

    boolean checkPointEquals(HitboxService other, int x, int y) {
        return contains(x, y) == other.contains(x, y);
    }

    @Override
    public int positionX() {
        return inner.positionX();
    }

    @Override
    public int positionY() {
        return inner.positionY();
    }

    @Override
    public boolean contains(int x, int y) {
        return inner.contains(x, y);
    }

    @Override
    public boolean intersects(HitboxService other) {
        return inner.intersects(other);
    }

    @Override
    public boolean equals(HitboxService other) {
        return inner.intersects(other);
    }

    @Override
    public void init(int x, int y) {
        inner.init(x, y);
        checkInvariants();

        if (inner.positionX() != x || inner.positionY() != y) {
            throw new PostconditionBroken("position after init");
        }
    }

    @Override
    public void moveTo(int x, int y) {
        boolean contains_origin = contains(positionX(), positionY());
        int[] relative = { rng.nextInt(100), rng.nextInt(100) };
        boolean contains_relative = contains(positionX() + relative[0],
                                             positionY() + relative[1]);
        int[] fixed = { rng.nextInt(300), rng.nextInt(300) };
        boolean contains_fixed = contains(fixed[0], fixed[1]);
        int[] move = { x - positionX(), y - positionY() };

        inner.moveTo(x, y);
        checkInvariants();

        if (inner.positionX() != x || inner.positionY() != y) {
            throw new PostconditionBroken("position after moveTo");
        }
        if ((contains(positionX(), positionY()) != contains_origin) ||
                (contains(positionX() + relative[0], positionY() + relative[1])
                        != contains_relative) ||
                (contains(fixed[0] + move[0], fixed[1] + move[1]) != contains_fixed)) {
            throw new PostconditionBroken("contains");
        }
    }
}
