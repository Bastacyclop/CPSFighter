package cps.fighter.kernel;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

public abstract class HitboxTest {
    HitboxService inner;
    Random rng;

    public HitboxTest() {
        inner = null;
        rng = new Random();
    }

    protected void setup(HitboxService inner) {
        this.inner = new HitboxContract(inner, rng);
    }

    @BeforeEach
    public abstract void beforeEach();

    @AfterEach
    public final void afterEach() {
        inner = null;
    }

    @Test
    public void testInitOk() {
        inner.init(rng.nextInt(1000), rng.nextInt(1000));
    }

    @Test
    public void testMoveToOk() {
        inner.init(rng.nextInt(1000), rng.nextInt(1000));
        inner.moveTo(rng.nextInt(1000), rng.nextInt(1000));
    }
}
