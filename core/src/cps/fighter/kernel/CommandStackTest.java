package cps.fighter.kernel;

import cps.fighter.PreconditionBroken;
import cps.fighter.kernel.Command;
import cps.fighter.kernel.CommandStackContract;
import cps.fighter.kernel.CommandStackService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static cps.fighter.kernel.Command.*;

public abstract class CommandStackTest {
    protected CommandStackService inner;

    public CommandStackTest() {
        inner = null;
    }

    protected void setup(CommandStackService inner) {
        this.inner = new CommandStackContract(inner);
    }

    @BeforeEach
    public abstract void beforeEach();

    @AfterEach
    public void afterEach() {
        this.inner = null;
    }

    @Test
    public void initOk() {
        inner.init(5);
    }

    @Test
    public void initNegativeSize() {
        try {
            inner.init(-5);
            assert(false);
        } catch (PreconditionBroken pb) {
        }
    }

    @Test
    public void pushIdle() {
        inner.init(5);
        inner.push(IDLE, false);
    }

    @Test
    public void pushCanCombo() {
        inner.init(5);
        inner.push(PUNCH, true);
    }

    @Test
    public void pushCannotCombo() {
        inner.init(5);
        inner.push(KICK, false);
    }

    @Test
    public void pushPopCombo() {
        Command[] cmds = { KICK, DOWN, DOWNRIGHT };
        inner.init(5);
        for (Command c : cmds) {
            inner.push(c, true);
        }
        assert(inner.popCombo(cmds));
    }

    @Test
    public void pushPopComboWithIdle() {
        Command[] cmds = { KICK, IDLE, IDLE, DOWN, DOWNRIGHT, IDLE };
        Command[] combo = Arrays.stream(cmds).filter(c -> c != IDLE)
                .toArray(Command[]::new);
        inner.init(5);
        for (Command c : cmds) {
            inner.push(c, c != IDLE);
        }
        assert(inner.popCombo(combo));
    }

    @Test
    public void popComboOverflow() {
        inner.init(3);
        try {
            inner.popCombo(new Command[]{KICK, LEFT, PUNCH, DOWN});
            assert(false);
        } catch (PreconditionBroken pb) {}
    }

    @Test
    public void popComboWithIdle() {
        inner.init(3);
        try {
            inner.popCombo(new Command[]{IDLE, LEFT});
            assert(false);
        } catch (PreconditionBroken pb) {}
    }
}
