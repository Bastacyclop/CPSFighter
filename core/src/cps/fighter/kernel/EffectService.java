package cps.fighter.kernel;

public interface EffectService {
    EffectType type();
    int value();

    void init(EffectType effect, int value);
}
