package cps.fighter.kernel;

public interface JumboService extends CharacterService {
    JumboState state();
    int fx();
    int timeLeft();
    RectangleHitboxService skillHitbox();
    boolean touched();

    void gotoIdle();
    void gotoJump(int x);
    void gotoMove(int x);
    void gotoCrouch(int x);
    void gotoPunch();
    void gotoKick();
    boolean gotoFireball();
    void stepIdle(Command c);
    void stepMove(Command c);
    void stepJump(Command c);
    void stepCrouch(Command c);
    void stepPunch(Command c);
    void stepKick(Command c);
    void stepFireball(Command c);
    void stepStun(Command c);
}
