package cps.fighter.kernel;

import cps.fighter.PreconditionBroken;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

public abstract class EngineTest {
    EngineService inner;
    Random rng;

    public EngineTest() {
        inner = null;
        rng = new Random();
    }

    protected void setup(EngineService inner) {
        this.inner = new EngineContract(inner);
    }

    @BeforeEach
    public abstract void beforeEach();

    @AfterEach
    public void afterEach() {
        this.inner = null;
    }

    @Test
    void testInitOk() {
        int space = rng.nextInt(100);
        inner.init(space + rng.nextInt(100 - space) + 200,
                   rng.nextInt(100) + 200,
                   space);
    }

    @Test
    void testInitNegativeHeight() {
        try {
            inner.init(10, -10, 5);
            assert(false);
        } catch (PreconditionBroken pre) {
        }
    }

    @Test
    void testInitNegativeWidth() {
        try {
            inner.init(-10, 5, rng.nextInt(20) - 10);
            assert(false);
        } catch (PreconditionBroken pre) {
        }
    }

    @Test
    void testInitOverSpaced() {
        try {
            int w = rng.nextInt(10);
            inner.init(w, rng.nextInt(20), w + rng.nextInt(10));
            assert (false);
        } catch (PreconditionBroken pre) {
        }
    }

    @Test
    void testLotsOfRandomSteps() {
        for (int i = 0; i < 10; i++) {
            testInitOk();
            for (int j = 0; j < 6666; j++) {
                if (inner.isGameOver()) {
                    break;
                }
                Command c0 = Command.values()[rng.nextInt(Command.values().length)];
                Command c1 = Command.values()[rng.nextInt(Command.values().length)];
                inner.step(c0, c0 != Command.IDLE && rng.nextBoolean(),
                        c1, c1 != Command.IDLE && rng.nextBoolean());
            }
        }
    }
}
