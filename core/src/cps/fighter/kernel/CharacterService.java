package cps.fighter.kernel;

import cps.fighter.Renderable;
import cps.fighter.impl.RectangleHitbox;

/*
    CharacterService Service,
    See the specification
 */
public interface CharacterService extends Renderable {
    int positionX();
    int positionY();
    RectangleHitboxService hitbox();
    GaugeService life();
    GaugeService stun();
    int speed();
    boolean isDown();
    boolean isCrouched();
    boolean canMove();
    boolean isProtected();
    boolean isGrounded();
    boolean isFacingRight();
    boolean isDead();
    EngineService engine();
    CharacterService opponent();
    CommandStackService cmdStack();
    RectangleHitboxService characterHitbox(int x, int y, int w, int h);
    RectangleHitboxService skillHitbox(int w, int h, int offsetx, int offsety);

    void init(int x, int y, EngineService e, int oid);

    void move(int fx, int fy);
    void applyEffect(EffectService e);
    void step(Command c, boolean can_combo);
}
