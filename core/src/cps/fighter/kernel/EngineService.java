package cps.fighter.kernel;

/*
    EngineService Service,
    See the specification
 */
public interface EngineService {
    int sceneWidth();
    int sceneHeight();
    CharacterService character(int c);
    boolean isGameOver();

    void init(int w, int h, int space);

    void step(Command c0, boolean cc0, Command c1, boolean cc1);
}
