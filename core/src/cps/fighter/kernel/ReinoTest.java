package cps.fighter.kernel;

import cps.fighter.impl.Engine;
import cps.fighter.impl.characters.Jumbo;
import cps.fighter.impl.characters.Reino;
import org.junit.jupiter.api.Test;

import static cps.fighter.kernel.Command.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class ReinoTest extends CharacterTest {
    @Override
    protected void setup(CharacterService inner) {
        this.inner = inner;
    }

    ReinoService get() {
        return (ReinoService) inner;
    }

    @Test
    void testCharge() {
        CharacterService[] opponents = { Jumbo.create(), Reino.create() };
        Command[] lcombo = {LEFT, LEFT, KICK};
        Command[] rcombo = {RIGHT, RIGHT, KICK};

        for (CharacterService opponent : opponents) {
            EngineService e = Engine.create(opponent, inner);
            e.init(200, 200, 50);

            e.step(IDLE, false, IDLE, false);

            for (Command c : rcombo) {
                e.step(IDLE, false, c, true);
            }
            assertTrue(get().state() == ReinoState.KICK, "should be KICK");

            for (int i = 0; i < 30; i++) {
                e.step(IDLE, false, IDLE, false);
            }

            for (Command c : lcombo) {
                e.step(IDLE, false, c, true);
            }
            assertTrue(get().state() == ReinoState.CHARGE, "should be CHARGE");

            for (int i = 0; i < 360; i++) {
                e.step(LEFT, false, IDLE, false);
            }
        }

        for (CharacterService opponent : opponents) {
            EngineService e = Engine.create(inner, opponent);
            e.init(200, 200, 50);

            e.step(IDLE, false, IDLE, false);

            for (Command c : lcombo) {
                e.step(c, true, IDLE, false);
            }
            assertTrue(get().state() == ReinoState.KICK, "should be KICK");

            for (int i = 0; i < 30; i++) {
                e.step(IDLE, false, IDLE, false);
            }

            for (Command c : rcombo) {
                e.step(c, true, IDLE, false);
            }
            assertTrue(get().state() == ReinoState.CHARGE, "should be CHARGE");

            for (int i = 0; i < 360; i++) {
                e.step(IDLE, false, RIGHT, false);
            }
        }
    }
}
