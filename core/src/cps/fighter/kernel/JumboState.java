package cps.fighter.kernel;

public enum JumboState {
    IDLE,
    CROUCH,
    MOVE,
    PUNCH,
    KICK,
    JUMP,
    STUN,

    FIREBALL,
}
