package cps.fighter.kernel;

import cps.fighter.PreconditionBroken;
import org.junit.jupiter.api.Test;

public abstract class RectangleHitboxTest extends HitboxTest {
    @Override
    protected void setup(HitboxService inner) {
        this.inner = new RectangleHitboxContract((RectangleHitboxService) inner, rng);
    }

    RectangleHitboxService get() {
        return (RectangleHitboxService)(inner);
    }

    @Test
    void testResizeOk() {
        get().init(rng.nextInt(1000), rng.nextInt(1000));
        get().resize(rng.nextInt(1000), rng.nextInt(1000));
    }

    @Test
    void testResizeNegative() {
        get().init(rng.nextInt(1000), rng.nextInt(1000));
        try {
            get().resize(-rng.nextInt(1000), -rng.nextInt(1000));
            assert (false);
        } catch (PreconditionBroken pre) {
        }
    }
}
