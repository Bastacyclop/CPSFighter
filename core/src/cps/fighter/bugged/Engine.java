package cps.fighter.bugged;

import cps.fighter.Config;
import cps.fighter.impl.*;
import cps.fighter.impl.Character;
import cps.fighter.impl.CommandStack;
import cps.fighter.impl.RectangleHitbox;
import cps.fighter.impl.characters.Jumbo;
import cps.fighter.kernel.*;

public class Engine implements EngineService {
    int scene_width = 0;
    int scene_height = 0;
    CharacterService[] character = { null, null };

    public Engine(CharacterService c0, CharacterService c1) {
        character[0] = c0;
        character[1] = c1;
    }

    public static EngineService create(CharacterService c0, CharacterService c1) {
        Engine e = new Engine(c0, c1);
        if (Config.CONTRACTS_ENABLED) {
            return new EngineContract(e);
        } else {
            return e;
        }
    }

    @Override
    public int sceneWidth() {
        return scene_width;
    }

    @Override
    public int sceneHeight() {
        return scene_height;
    }

    @Override
    public CharacterService character(int c) {
        return character[c];
    }

    @Override
    public boolean isGameOver() {
        return character[0].isDead() || character[1].isDead();
    }

    @Override
    public void init(int w, int h, int space) {
        scene_width = w;
        scene_height = h;

        character[0].init(w/2 - space/2, 0, this, 1);
        character[1].init(w/2 + space/2, 0, this, 0);
    }

    @Override
    public void step(Command c0, boolean cc0, Command c1, boolean cc1) {
        character[0].step(c0, cc0);
        character[1].step(c1, cc1);
        handleCollisions();
    }

    void handleCollisions() {
        RectangleHitboxService h0 = character[0].hitbox();
        RectangleHitboxService h1 = character[1].hitbox();

        if (h0.intersects(h1)) {
            int sup0 = h0.positionX() + h0.width();
            int inf1 = h1.positionX();

            if (sup0 >= inf1) {
                character[0].move(-1, 0);
                character[1].move(1, 0);
            } else {
                // sup1 >= inf0
                character[1].move(-1, 0);
                character[0].move(1, 0);
            }
        }
    }
}
