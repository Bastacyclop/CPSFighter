package cps.fighter.bugged;
import cps.fighter.bugged.CommandStack;
import org.junit.jupiter.api.BeforeEach;

public class CommandStackTest extends cps.fighter.kernel.CommandStackTest {
    @Override
    @BeforeEach
    public void beforeEach() {
        setup(new CommandStack());
    }
}
