package cps.fighter.bugged;

import cps.fighter.kernel.GaugeService;

public class Gauge implements GaugeService {
    int value;
    int limit;

    @Override
    public int value() {
        return value;
    }

    @Override
    public int limit() {
        return limit;
    }

    @Override
    public void init(int v, int l) {
        value = v;
        limit = l;
    }

    @Override
    public void add(int a) {
        value += a;
    }
}
