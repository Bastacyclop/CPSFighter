package cps.fighter.bugged;

import cps.fighter.Config;
import cps.fighter.kernel.Command;
import cps.fighter.kernel.CommandStackContract;
import cps.fighter.kernel.CommandStackService;

public class CommandStack implements CommandStackService {
    protected Command[] ring;
    protected int head;
    protected int amount;
    protected boolean idle;

    public static CommandStackService create() {
        CommandStack s = new CommandStack();
        if (Config.CONTRACTS_ENABLED) {
            return new CommandStackContract(s);
        } else {
            return s;
        }
    }

    @Override
    public int size() {
        return ring.length;
    }

    @Override
    public Command last() {
        if (idle || amount == 0) {
            return Command.IDLE;
        } else {
            return ring[head];
        }
    }

    @Override
    public boolean popCombo(Command[] combo) {
        if (amount < combo.length) {
            return false;
        }

        int i = (head + ring.length - combo.length + 1) % ring.length;
        for (Command c : combo) {
            if (ring[i] != c) {
                return false;
            }
            i = (i + 1) % ring.length;
        }
        head = i;
        amount -= combo.length;
        return true;
    }

    @Override
    public void init(int size) {
        ring = new Command[size];
        head = 0;
        amount = 0;
        idle = true;
    }

    @Override
    public void push(Command c, boolean can_combo) {
        if (c == Command.IDLE) {
            idle = true;
        } else {
            head = (head + 1) % ring.length;
            ring[head] = c;
            amount += 1;
            if (amount > ring.length) {
                amount = ring.length;
            }
            idle = false;
        }
    }
}
