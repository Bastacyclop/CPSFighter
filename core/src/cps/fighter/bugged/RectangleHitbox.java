package cps.fighter.bugged;

import cps.fighter.kernel.HitboxService;
import cps.fighter.kernel.RectangleHitboxService;

public class RectangleHitbox implements RectangleHitboxService {
    int[] inf = {0, 0};
    int[] sup = {1, 1};

    @Override
    public int positionX() {
        return inf[0];
    }

    @Override
    public int positionY() {
        return inf[1];
    }

    @Override
    public int width() {
        return sup[0] - inf[0];
    }

    @Override
    public int height() {
        return sup[1] - inf[1];
    }

    @Override
    public boolean contains(int x, int y) {
        return inf[0] <= x && x < sup[0] &&
                inf[1] < y && y < sup[1];
    }

    @Override
    public boolean intersects(HitboxService other) {
        // TODO: how the fuck am I supposed to implement this ?
        return false;
    }

    @Override
    public boolean equals(HitboxService other) {
        // TODO: how the fuck am I supposed to implement this ?
        return false;
    }

    @Override
    public void init(int x, int y) {
        moveTo(x, y);
    }

    @Override
    public void moveTo(int x, int y) {
        inf[0] = x;
        inf[1] = y;
    }

    @Override
    public void resize(int w, int h) {
        sup[0] = inf[0] + w;
        sup[1] = inf[1] + h;
    }
}