package cps.fighter.bugged;

import cps.fighter.kernel.EffectService;
import cps.fighter.kernel.EffectType;

public class Effect implements EffectService {
    protected EffectType effect;
    protected int value;

    @Override
    public EffectType type() {
        return effect;
    }

    @Override
    public int value() {
        return value;
    }

    @Override
    public void init(EffectType e, int v) {
        effect = e;
    }
}
