package cps.fighter.bugged.characters;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import cps.fighter.Config;
import cps.fighter.Game;
import cps.fighter.impl.Character;
import cps.fighter.impl.*;
import cps.fighter.kernel.*;

public class Jumbo extends Character implements JumboService {
    JumboService proxy;
    JumboState state;

    public Jumbo() {
        super("Jumbo", new Gauge(), new Gauge(), 2, new CommandStack());
        proxy = this;
    }

    public static CharacterService create_with_contract() {
        Jumbo j = new Jumbo();
        JumboContract c = new JumboContract(j);
        j.proxy = c;
        return c;
    }

    public static CharacterService create() {
        if (Config.CONTRACTS_ENABLED) {
            return create_with_contract();
        } else {
            return new Jumbo();
        }
    }

    @Override
    public boolean isCrouched() {
        return state == JumboState.CROUCH;
    }

	@Override
	public boolean canMove() {
		return state == JumboState.IDLE || state == JumboState.CROUCH;
	}

    @Override
    public void init(int x, int y, EngineService e, int oid) {
        super.init(x, y, e, oid);
        life.init(100, 100);
        stun.init(0, 300);
        proxy.gotoIdle();
        skill_hitbox = RectangleHitbox.create(0, 0, 0, 0);
    }

    @Override
    public JumboState state() {
        return state;
    }

    @Override
    public int fx() {
        return fx;
    }

    @Override
    public int timeLeft() {
        return time_left;
    }

    @Override
    public RectangleHitboxService skillHitbox() {
        return skill_hitbox;
    }

    @Override
    public boolean touched() {
        return touched;
    }

    public void gotoIdle() {
        state = JumboState.IDLE;
        hitbox = RectangleHitbox.create(positionX(), positionY(), 50, 80);
        is_protected = false;
    }

    public void gotoJump(int x) {
        state = JumboState.JUMP;
        fx = x;
        time_left = 30;
    }

    public void gotoMove(int x) {
        state = JumboState.MOVE;
        is_protected = (is_facing_right == (x < 0));
        fx = x;
    }

    public void gotoCrouch(int x) {
        state = JumboState.CROUCH;
        is_protected = (is_facing_right == (x < 0));
        hitbox = RectangleHitbox.create(positionX(), positionY(), 52, 40);
    }

    public void gotoPunch() {
        state = JumboState.PUNCH;
        time_left = 10;
        setSkillHitbox(40, 10, 20, 45);
        touched = false;
    }

    public void gotoKick() {
        state = JumboState.KICK;
        time_left = 30;
        setSkillHitbox(60, 10, 20, 10);
        touched = false;
    }

    public boolean gotoFireball() {
        if (is_facing_right) {
            if (cmd_stack.popCombo(new Command[] {
                    Command.DOWN, Command.DOWNRIGHT, Command.RIGHT, Command.PUNCH })) {
                state = JumboState.FIREBALL;
                fx = 2;
            }
        } else {
            if (cmd_stack.popCombo(new Command[] {
                    Command.DOWN, Command.DOWNLEFT, Command.LEFT, Command.PUNCH })) {
                state = JumboState.FIREBALL;
                fx = -2;
            }
        }
        if (state == JumboState.FIREBALL) {
            time_left = 90;
            setSkillHitbox(40, 30, 40, 52);
            return true;
        }
        return false;
    }

    @Override
    public void mayGotoStun() {
        if (state != JumboState.STUN) {
            state = JumboState.STUN;
            time_left = 120;
        }
    }

    @Override
    public void mayGotoDown() {
        if (state != JumboState.STUN) {
            state = JumboState.STUN;
            time_left = 120;
            hitbox = RectangleHitbox
                    .create(positionX(), positionY(), hitbox.height(), hitbox.width());
        }
    }

    public void stepIdle(Command cmd) {
        if (proxy.gotoFireball()) {
            return;
        }

        if (isGrounded()) {
            switch (cmd) {
                case UP:
                    proxy.gotoJump(0);
                    return;
                case UPLEFT:
                    proxy.gotoJump(-2);
                    return;
                case UPRIGHT:
                    proxy.gotoJump(2);
                    return;
                default:
                    break;
            }
        }

        switch (cmd) {
            case LEFT: proxy.gotoMove(-1);
                break;
            case RIGHT: proxy.gotoMove(1);
                break;
            case DOWN: proxy.gotoCrouch(0);
                break;
            case DOWNLEFT: proxy.gotoCrouch(-1);
                break;
            case DOWNRIGHT: proxy.gotoCrouch(1);
                break;
            case PUNCH: proxy.gotoPunch();
                break;
            case KICK: proxy.gotoKick();
                break;
        }
    }

    public void stepMove(Command cmd) {
        proxy.move(fx * speed, 0);
        proxy.gotoIdle();
        proxy.stepIdle(cmd);
    }

    public void stepJump(Command cmd) {
        if (time_left > 0) {
            time_left--;
            proxy.move(fx * speed, 8);
        } else {
            proxy.move(fx * speed, 0);
            if (isGrounded()) {
                proxy.gotoIdle();
            }
        }
    }

    public void stepCrouch(Command cmd) {
        proxy.gotoIdle();
        proxy.stepIdle(cmd);
    }

    public void stepPunch(Command cmd) {
        if (time_left > 0) {
            time_left--;
            if (!touched && opponent().hitbox().intersects(skill_hitbox)) {
                touched = true;
                if (isFacingRight()) {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, 5));
                } else {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, -5));
                }
                opponent().applyEffect(Effect.create(EffectType.OVERHEAD, 2));
                opponent().applyEffect(Effect.create(EffectType.STUN, 80));
            }
        } else {
            proxy.gotoIdle();
            proxy.stepIdle(cmd);
        }
    }

    public void stepKick(Command cmd) {
        if (time_left > 0) {
            time_left--;
            if (!touched && opponent().hitbox().intersects(skill_hitbox)) {
                touched = true;
                if (isFacingRight()) {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, 10));
                } else {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, -10));
                }
                opponent().applyEffect(Effect.create(EffectType.DAMAGE, 5));
                opponent().applyEffect(Effect.create(EffectType.STUN, 100));
            }
        } else {
            proxy.gotoIdle();
            proxy.stepIdle(cmd);
        }
    }

    public void stepStun(Command c) {
        if (time_left > 0) {
            time_left--;
            stun.add(stun.limit());
        } else {
            stun.add(-stun.limit());
            proxy.gotoIdle();
            proxy.stepIdle(c);
        }
    }

    public void stepFireball(Command c) {
        if (time_left > 0) {
            time_left--;
            skill_hitbox.moveTo(skill_hitbox.positionX() + fx * speed, skill_hitbox.positionY());
            if (opponent().hitbox().intersects(skill_hitbox)) {
                if (isFacingRight()) {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, 1));
                } else {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, -1));
                }
                opponent().applyEffect(Effect.create(EffectType.DAMAGE, 2));
                opponent().applyEffect(Effect.create(EffectType.STUN, 5));
            }
        } else {
            proxy.gotoIdle();
            proxy.stepIdle(c);
        }
    }

    @Override
    public void step() {
        Command c = cmd_stack.last();
        switch (state) {
            case IDLE: proxy.stepIdle(c);
                break;
            case CROUCH: proxy.stepCrouch(c);
                break;
            case MOVE: proxy.stepMove(c);
                break;
            case JUMP: proxy.stepJump(c);
                break;
            case PUNCH: proxy.stepPunch(c);
                break;
            case KICK: proxy.stepKick(c);
                break;
            case STUN: proxy.stepStun(c);
                break;

            case FIREBALL: proxy.stepFireball(c);
                break;
        }
    }

    @Override
    public void move(int x, int y) {
        super.move(x, y);

        switch (state) {
            case KICK:
            case PUNCH:
                skill_hitbox.moveTo(skill_hitbox.positionX() + x, skill_hitbox.positionY() + y);
                break;
        }
    }

    @Override
    public void render(Game g) {
        g.renderer.begin(ShapeRenderer.ShapeType.Filled);
        RectangleHitboxService h = hitbox;
        if (is_protected) {
            g.renderer.setColor(0.7f, 0.36f, 0.25f, 1);
        } else {
            g.renderer.setColor(0.7f, 0.82f, 0.25f, 1);
        }
        g.renderHitbox(h);
        g.renderer.setColor(0.2f, 0.2f, 0.2f, 1);
        g.renderEye(this);
        switch (state) {
            case PUNCH:
            case KICK:
            case FIREBALL:
                g.renderer.setColor(0.7f, 0.36f, 0.25f, 1);
                g.renderHitbox(skill_hitbox);
                break;
        }
        g.renderer.end();
    }
}
