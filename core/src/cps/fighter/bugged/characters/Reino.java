package cps.fighter.bugged.characters;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import cps.fighter.Config;
import cps.fighter.Game;
import cps.fighter.impl.Character;
import cps.fighter.impl.*;
import cps.fighter.kernel.*;

public class Reino extends Character implements ReinoService {
    ReinoService proxy;
    ReinoState state;

    public Reino() {
        super("Reino", new Gauge(), new Gauge(), 2, new CommandStack());
        proxy = this;
    }

    public static CharacterService create_with_contract() {
        Reino r = new Reino();
        ReinoContract c = new ReinoContract(r);
        r.proxy = c;
        return c;
    }

    public static CharacterService create() {
        if (Config.CONTRACTS_ENABLED) {
            return create_with_contract();
        } else {
            return new Reino();
        }
    }

    @Override
    public boolean isCrouched() {
        return state == ReinoState.CROUCH;
    }

	@Override
	public boolean canMove() {
		return state == ReinoState.IDLE || state == ReinoState.CROUCH;
	}

    @Override
    public void init(int x, int y, EngineService e, int oid) {
        super.init(x, y, e, oid);
        life.init(150, 150);
        stun.init(0, 320);
        proxy.gotoIdle();
        skill_hitbox = RectangleHitbox.create(0, 0, 0, 0);
    }

    @Override
    public ReinoState state() {
        return state;
    }

    @Override
    public int fx() {
        return fx;
    }

    @Override
    public int timeLeft() {
        return time_left;
    }

    @Override
    public RectangleHitboxService skillHitbox() {
        return skill_hitbox;
    }

    @Override
    public boolean touched() {
        return touched;
    }

    public void gotoIdle() {
        hitbox = RectangleHitbox.create(positionX(), positionY(), 60, 90);
        state = ReinoState.IDLE;
        is_protected = false;
    }

    public void gotoJump(int x) {
        state = ReinoState.JUMP;
        fx = x;
        time_left = 40;
    }

    public void gotoMove(int x) {
        is_protected = (x != 0) && (is_facing_right == (x < 0));
        state = ReinoState.MOVE;
        fx = x;
    }

    public void gotoCrouch(int x) {
        is_protected = (x != 0) && (is_facing_right == (x < 0));
        state = ReinoState.CROUCH;
        hitbox = RectangleHitbox.create(positionX(), positionY(), 70, 50);
    }

    public void gotoPunch() {
        state = ReinoState.PUNCH;
        time_left = 20;
        setSkillHitbox(50, 20, 20, 50);
        touched = false;
    }

    public void gotoKick() {
        state = ReinoState.KICK;
        time_left = 10;
        setSkillHitbox(30, 20, 20, 10);
        touched = false;
    }

    public boolean gotoCharge() {
        if (cmd_stack.popCombo(new Command[] {
                Command.RIGHT, Command.RIGHT, Command.KICK })) {
            state = ReinoState.CHARGE;
            fx = 3;
            time_left = 120;
            setSkillHitbox(10, 60, 30, 10);
            return true;
        }
        return false;
    }

    @Override
    public void mayGotoStun() {
        if (state != ReinoState.STUN) {
            state = ReinoState.STUN;
            time_left = 120;
        }
    }

    @Override
    public void mayGotoDown() {
        if (state != ReinoState.STUN) {
            state = ReinoState.STUN;
            time_left = 120;
            hitbox = RectangleHitbox
                    .create(positionX(), positionY(), hitbox.height(), hitbox.width());
        }
    }

    public void stepIdle(Command cmd) {
        if (proxy.gotoCharge()) {
            return;
        }

        if (isGrounded()) {
            switch (cmd) {
                case UP:
                    proxy.gotoJump(0);
                    return;
                case UPLEFT:
                    proxy.gotoJump(-2);
                    return;
                case UPRIGHT:
                    proxy.gotoJump(2);
                    return;
                default:
                    break;
            }
        }

        switch (cmd) {
            case LEFT: proxy.gotoMove(-1);
                break;
            case RIGHT: proxy.gotoMove(1);
                break;
            case DOWN: proxy.gotoCrouch(0);
                break;
            case DOWNLEFT: proxy.gotoCrouch(-1);
                break;
            case DOWNRIGHT: proxy.gotoCrouch(1);
                break;
            case PUNCH: proxy.gotoPunch();
                break;
            case KICK: proxy.gotoKick();
                break;
        }
    }

    public void stepMove(Command cmd) {
        proxy.move(fx * speed, 0);
        proxy.gotoIdle();
        proxy.stepIdle(cmd);
    }

    public void stepJump(Command cmd) {
        if (time_left > 0) {
            time_left++;
            proxy.move(fx * speed, 7);
        } else {
            proxy.move(fx * speed, 0);
            if (isGrounded()) {
                proxy.gotoIdle();
            }
        }
    }

    public void stepCrouch(Command cmd) {
        proxy.gotoIdle();
        proxy.stepIdle(cmd);
    }

    public void stepPunch(Command cmd) {
        if (time_left > 0) {
            time_left--;
            if (!touched && opponent().hitbox().intersects(skill_hitbox)) {
                if (isFacingRight()) {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, 15));
                } else {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, -15));
                }
                opponent().applyEffect(Effect.create(EffectType.DAMAGE, 5));
                opponent().applyEffect(Effect.create(EffectType.STUN, 80));
            }
        } else {
            proxy.gotoIdle();
            proxy.stepIdle(cmd);
        }
    }

    public void stepKick(Command cmd) {
        if (time_left > 0) {
            time_left--;
            if (!touched && opponent().hitbox().intersects(skill_hitbox)) {
                if (isFacingRight()) {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, 5));
                } else {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, -5));
                }
                opponent().applyEffect(Effect.create(EffectType.LOW, 2));
                opponent().applyEffect(Effect.create(EffectType.STUN, 10));
            }
        } else {
            proxy.gotoIdle();
            proxy.stepIdle(cmd);
        }
    }

    public void stepStun(Command c) {
        if (time_left > 0) {
            time_left--;
            stun.add(stun.limit());
        } else {
            stun.add(-stun.limit());
            is_down = false;
            proxy.gotoIdle();
            proxy.stepIdle(c);
        }
    }

    public void stepCharge(Command c) {
        if (time_left > 0) {
            time_left--;
            proxy.move(fx * speed, 0);
            if (opponent().hitbox().intersects(skill_hitbox)) {
                if (isFacingRight()) {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, 1));
                } else {
                    opponent().applyEffect(Effect.create(EffectType.PUSH, -1));
                }
                opponent().applyEffect(Effect.create(EffectType.DAMAGE, time_left%2));
                opponent().applyEffect(Effect.create(EffectType.KNOCKDOWN, 0));
            }
        } else {
            proxy.gotoIdle();
            proxy.stepIdle(c);
        }
    }

    @Override
    public void step() {
        Command c = cmd_stack.last();
        switch (state) {
            case IDLE: proxy.stepIdle(c);
                break;
            case CROUCH: proxy.stepCrouch(c);
                break;
            case MOVE: proxy.stepMove(c);
                break;
            case JUMP: proxy.stepJump(c);
                break;
            case PUNCH: proxy.stepPunch(c);
                break;
            case KICK: proxy.stepKick(c);
                break;
            case STUN: proxy.stepStun(c);
                break;

            case CHARGE: proxy.stepCharge(c);
                break;
        }
    }

    @Override
    public void move(int x, int y) {
        super.move(x, y);

        switch (state) {
            case KICK:
            case PUNCH:
            case CHARGE:
                skill_hitbox.moveTo(skill_hitbox.positionX() + x, skill_hitbox.positionY() + y);
                break;
        }
    }

    @Override
    public void render(Game g) {
        g.renderer.begin(ShapeRenderer.ShapeType.Filled);
        RectangleHitboxService h = hitbox;
        if (is_protected) {
            g.renderer.setColor(0.45f, 0.42f, 0.7f, 1);
        } else {
            g.renderer.setColor(0.47f, 0.64f, 0.8f, 1);
        }
        g.renderHitbox(h);
        g.renderer.setColor(0.2f, 0.2f, 0.2f, 1);
        g.renderEye(this);
        switch (state) {
            case PUNCH:
            case KICK:
            case CHARGE:
                g.renderer.setColor(0.45f, 0.42f, 0.7f, 1);
                g.renderHitbox(skill_hitbox);
                break;
        }
        g.renderer.end();
    }
}
