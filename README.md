# CPS Fighter

This project is using [libgdx](https://libgdx.badlogicgames.com/) and [gradle](https://gradle.org/).

## Usage

With the command line, you need `JAVA_HOME` to be set to your JDK (7+):
```
cd CPSFighter
# compile the project
./gradlew desktop:build
# run the project
./gradlew desktop:run
# run unit tests
./test.sh package
```
